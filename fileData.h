#ifndef FILEDATA_H
#define FILEDATA_H
#include <string>
#include <vector>
#include <fstream>
class fileData{
public:
	std::string strFileDirectory;
	std::string strFileName;
	std::string strFileInfo;
	bool highlight;
	int delay;
	fileData(std::string input, std::string directory);
	std::string print();
};
std::string getLastFile(std::vector<fileData> files,std::string fileName);
std::string incrementFile(std::string);
#endif
