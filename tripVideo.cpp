#include <ncurses.h>
#include <iostream>
#include <fstream>
#include <vector>
#include "utils.h"
#include "opPreProcess.h"
#include "fileData.h"
using namespace std;


//Store the values for the window size
int x,y;

//This function returns the autocompleted string.
string getAuto(vector<string> &input, string userline){
	if(userline.length()==0)
		return "";
	string retString="";
	int sLength=99999999;
	//Find shortest string
	for(int i=0;i<input.size();i++)
		if(input[i].length()<sLength)
			sLength=input[i].length();
	for(int i=userline.length();i<sLength;i++){
		bool add=true;
		char c=input[0][i];
		for(int j=0;j<input.size();j++){
			if(input[j][i]!=c)
				add=false;
		}
		if(add)
			retString+=input[0][i];
		else
			return retString;
	}
	return retString;
}


vector<fileData> fileSearch(vector<fileData>& data,vector<string>& directory){
	vector<fileData> matchingFiles;
	if(data.size()>directory.size())
		return matchingFiles;
	for(int i;i<directory.size();i++)
		if(directory[i]==data[i].strFileName)
			matchingFiles.push_back(data[i]);
	return matchingFiles;
}

string getDir(string fileName, const vector<fileData>& data){
	for(int i=0;i<data.size();i++)
		if(data[i].strFileName==fileName)
			return "./"+data[i].strFileDirectory+"/"+data[i].strFileName;
	return "not found";
}

void getFileInfo(vector<fileData>& out, string directory,char splitter='\n'){
	string output = "./getVids " + directory;
	system(output.c_str());
	ifstream fileList("/tmp/fileInfo");
	//vector<fileData> out;
	while(fileList.good()){
		string s;
		getline(fileList,s,splitter);
		//cerr << s;
		if(s.find("|")!=-1){
			fileData file(s,directory);
			out.push_back(file);
		}
	}
}


void getCommandHistory(vector<string>& file ,char splitter='\n'){
	ifstream fileList("./command_history");
	//vector<fileData> out;
	while(fileList.good()){
		string s;
		getline(fileList,s,splitter);
		file.push_back(s);
		//cout << s << endl;
	}
}

int main()
{	
	initscr();			/* Start curses mode*/
	noecho();
	halfdelay(1);
	//gets rid of text echo
	keypad(stdscr, TRUE);
	//Creates char for input
	int c=-1;
	vector<fileData> footageInfo;
	vector<string> commandHistory;
	getCommandHistory(commandHistory);
	int commandPosition=commandHistory.size();
	//Load command history
	//This part creates footageInfo that contains all the footage files.
	getFileInfo(footageInfo,"footage");
	getFileInfo(footageInfo,"work");

	int cursorPosition=0;
	//SelectedFileIndex
	string retStr="";
	//Used for storing errors or other comments
	string message="";


	do{

		/*Warning y and x are global*/
		getmaxyx(stdscr, y,x);
		//Create all the windows
		WINDOW * leftWin = newwin(y/5*4,x/2,0,0);//Footage directory
		WINDOW * rightWin = newwin(y/5*4,x/2,0,x/2);//Work directory
		WINDOW * botWin = newwin(y/5,x,y/5*4,0);//Command line
		//This prints the files in the dialog and work box
		wprintw(leftWin,"Footage:\n");
		wprintw(rightWin,"Workbox:\n");
		if(footageInfo.size()==0)
			wprintw(leftWin,"-No current files");
		else
			for(int i=0;i<footageInfo.size();i++){
				if(footageInfo[i].strFileDirectory=="footage")
					if(footageInfo[i].highlight){
						wattron(leftWin,A_REVERSE);
						wprintw(leftWin,footageInfo[i].print().c_str());
					}
					else{
						wattroff(leftWin,A_REVERSE);
						wprintw(leftWin,footageInfo[i].print().c_str());
					}
				if(footageInfo[i].strFileDirectory=="work")
					if(footageInfo[i].highlight){
						wattron(rightWin,A_REVERSE);
						wprintw(rightWin,footageInfo[i].print().c_str());
					}
					else{
						wattroff(rightWin,A_REVERSE);
						wprintw(rightWin,footageInfo[i].print().c_str());
					}
			}
		//Refresh all the windows
		wrefresh(leftWin);
		wrefresh(rightWin);
		refresh();
		//Creates vector for storing command list
		vector<string> keywords;
		keywords.push_back("cut");
		keywords.push_back("play");
		keywords.push_back("smooth");
		keywords.push_back("insert");
		keywords.push_back("concat");
		keywords.push_back("sconcat");
		keywords.push_back("help");
		keywords.push_back("quit");
		keywords.push_back("add");
		keywords.push_back("subtract");
		keywords.push_back("snip");
		keywords.push_back("audiosource");
		keywords.push_back("remove");
		keywords.push_back("cr_insert");
		//These are handled by the preprocessor
		keywords.push_back("start");
		keywords.push_back("end");
		keywords.push_back("last");
		//Handles autocomplete I wrote this when I was much less then sober I have no clue how it works
		if(c=='\t' && retStr.length()!=0){
			//Creates vector for storing potential keywords and locations int the previous array
			vector<string> potentialFiles;
			string temp="";
			//Finds the word to autocomplete stored in temp
			int originalCursorPosition=cursorPosition;
			while(cursorPosition>=retStr.length())cursorPosition--;
			for(;cursorPosition>0 && retStr[cursorPosition-1]!=' ';cursorPosition--);
			for(;cursorPosition<retStr.length() && retStr[cursorPosition]!=' ' && cursorPosition<originalCursorPosition;cursorPosition++){
				temp+=retStr[cursorPosition];
			}
			//Runs the compare function to find potential matches
			for(int i=0;i<footageInfo.size();i++){
				//This highlights and autocompletes the keywords
				if(compare(temp,footageInfo[i].strFileName)){
					//Adds the files to the list
					potentialFiles.push_back(footageInfo[i].strFileName);
					//Highlights the files
					footageInfo[i].highlight=true;
				}
				else
					footageInfo[i].highlight=false;

			}
			for(int i=0;i<keywords.size();i++)
				if(compare(temp,keywords[i]))
					potentialFiles.push_back(keywords[i]);
	
			if(potentialFiles.size()>=1){
				retStr.insert(cursorPosition,getAuto(potentialFiles,temp));
				cursorPosition+=getAuto(potentialFiles,temp).length();
				werase(botWin);
				wrefresh(botWin);
			}
		}

		/*Exits loop on enter this also handles running commands*/
		if(c=='\n'){
			writeCommandHistory(retStr);
			bool isValid=true;
			vector<string> operands=string_split(retStr,' ');
			opPreProcess(operands,footageInfo);
			if(operands[0]==keywords[0] && operands.size()==4){
				string command="./cut";
				string fileName="CU_-1.mkv";
				command+=" "+operands[2]+" "+operands[3]+" "+getDir(operands[1],footageInfo)+" ./work/"+incrementFile(getLastFile(footageInfo,fileName));
				writeCommand(command);
				writeTest(command);
				endwin();//End curses mode
				return 0;
			}
			else if(operands[0]==keywords[3] && operands.size()==4){
				string command="./insert ";
				int fileLocation;
				for(int i=0;i<footageInfo.size();i++)
					if(footageInfo[i].strFileName==operands[1])
						fileLocation=i;

				/*for(int i=0;i<footageInfo.size();i++)
					if(footageInfo[i].strFileName=="source_audio.wav" && stod(string_split(diff_time_codes(footageInfo[fileLocation].strFileInfo, footageInfo[i].strFileInfo,  subtract_doubles),':')[2])>0.1)
						command+="cr_insert";
					else
						command+="insert"*/;

				string fileName="IN_-1.mkv";
				command+=operands[3]+" "+getDir(operands[1],footageInfo)+" "+getDir(operands[2],footageInfo)+" ./work/"+incrementFile(getLastFile(footageInfo,fileName));
				writeCommand(command);
				writeTest(command);
				endwin();//End curses mode
				return 0;
			}
			else if(operands[0]==keywords[2] && operands.size()==2){
				string fileName="SM_-1.mkv";
				string command="./smooth "+getDir(operands[1],footageInfo)+" ./work/"+incrementFile(getLastFile(footageInfo,fileName));
				writeCommand(command);
				endwin();//End curses mode
				return 0;
			}
			else if(operands[0]==keywords[1] && operands.size()==2){
				string command="( mpv "+getDir(operands[1],footageInfo)+ " 2>/dev/null & )";
				writeCommand(command);
				endwin();//End curses mode
				return 0;
			}
			else if(operands[0]==keywords[4] && operands.size()>2){
				string fileName="CN_-1.mkv";
				string command="./concatinate -o ./work/"+incrementFile(getLastFile(footageInfo,fileName));
				for(int i=1;i<operands.size()-1;i++)
					command+=" "+getDir(operands[i],footageInfo);
				writeCommand(command);
				writeTest(command);
				endwin();//End curses mode
				return 0;

			}
			else if(operands[0]==keywords[5] && operands.size()>2){
				string fileName="SC_-1.mkv";
				string command="./smooth_concatinate -o ./work/"+incrementFile(getLastFile(footageInfo,fileName));
				for(int i=2;i<operands.size()-1;i++)
					command+=" "+getDir(operands[i],footageInfo);
				writeCommand(command);
				writeTest(command);
				endwin();//End curses mode
				return 0;
			}
			else if(operands[0]==keywords[6]){
				message="\ncut:(cuts video) filename (start)timecode (end)timecode\n" \
					 "play:(runs mpv) filename\n" \
					 "smooth:(smooths audio) filename (length)timecode (length)timecode\n" \
					 "insert: mainvideo clip tc\n" \
					 "concat:(concatinates video) filename... filename... filename.. ...\n" \
					 "sconcat:(smoothly concatinates video) filename...\n" \
					 "add, subtract: tc tc";
				isValid=false;
				//writeCommand("read -p \"Press enter to continue\"");
			}
			else if(operands[0]==keywords[7]){
				endwin();
				writeCommand("echo bye!\nstty sane");
				return 0;
			}
			else if(operands[0]==keywords[8] && operands.size()==3){
				message="\n"+diff_time_codes(operands[1], operands[2], add_doubles);
				isValid=false;
			}
			else if(operands[0]==keywords[9] && operands.size()==3){
				message="\n"+diff_time_codes(operands[1], operands[2], subtract_doubles);
				isValid=false;
			}
			else if(operands[0]==keywords[10] && operands.size()==4){
				string fileName="SN_-1.mkv";
				string command="./snip " + operands[2] + " " + operands[3] + " " + operands[1] + "./work/"+incrementFile(getLastFile(footageInfo,fileName));
				writeCommand(command);
			}
			else if(operands[0]==keywords[11] && operands.size()==2){
				string command="./grabAudio " + operands[1];
				writeCommand(command);
			}
			else if(operands[0]==keywords[12] && operands.size()>=2){
				string command="rm";
				for(int i=2;i<operands.size();i++)
					command+=" "+getDir(operands[i],footageInfo);
				writeCommand(command);
				writeTest(command);
			}
			else if(operands[0]==keywords[3] && operands.size()==4){
				string command="./cr_insert ";
				int fileLocation;
				for(int i=0;i<footageInfo.size();i++)
					if(footageInfo[i].strFileName==operands[1])
						fileLocation=i;
						string fileName="IN_-1.mkv";
				command+=operands[3]+" "+getDir(operands[1],footageInfo)+" "+getDir(operands[2],footageInfo)+" ./work/"+incrementFile(getLastFile(footageInfo,fileName));
				writeCommand(command);
				writeTest(command);
				endwin();//End curses mode
				return 0;
			}
			else{
				message="\nInvalid Command or syntax: "+operands[0];
				isValid=false;
			}
			if(isValid){
				retStr="";
				werase(botWin);
				wrefresh(botWin);
				continue;
			}
		}
		else if(c==-1);
		else if(c==KEY_BACKSPACE){
			if(retStr.length()>0 && cursorPosition!=0){
				cursorPosition--;
				retStr.erase(cursorPosition,1);
			}
			werase(botWin);
			wrefresh(botWin);
		}
		else if(c==KEY_DC){
			if(retStr.length()>0){
				retStr.erase(cursorPosition,1);
			}
			werase(botWin);
			wrefresh(botWin);
		}
		//right arrow
		else if(c==KEY_RIGHT){
			if(cursorPosition<retStr.length()){
				cursorPosition++;
			}
		}
		//left arrow
		else if(c==KEY_LEFT){
			if(cursorPosition>0){
				cursorPosition--;
			}
		}
		//Up arrow
		else if(c==KEY_UP){
			if(commandPosition>0)
				commandPosition--;
			retStr=commandHistory[commandPosition];
			cursorPosition=retStr.length();
			//retStr="dsaf";
		}
		//Down arrow
		else if(c==KEY_DOWN){
			if(commandPosition<commandHistory.size()-1)
				commandPosition++;
			retStr=commandHistory[commandPosition];
			cursorPosition=retStr.length();
		}
		//Adds the characters to the return stringi*/
		else if(c!='\t' && c>0 && c<256){
			retStr.insert(cursorPosition,1,c);
			wrefresh(botWin);
			cursorPosition++;
		}
		wprintw(botWin,retStr.c_str());
		wprintw(botWin,message.c_str());
		wmove(botWin,cursorPosition/x,cursorPosition%x);
		wrefresh(botWin);
	}while(c=getch());
	endwin();//End curses mode

	return 0;
}
