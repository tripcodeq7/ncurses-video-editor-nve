#ifndef UTILS_H
#define UTILS_H
#include <string>
#include <vector>
#include <fstream>
std::vector<std::string> string_split(std::string, char);
bool compare(std::string, std::string);
void writeCommand(std::string);
void writeCommandHistory(std::string);
void writeTest(std::string);
double add_doubles(double d1, double d2);
double subtract_doubles(double d1, double d2);
double mult99_doubles(double d1, double d2);
void removeTrailingZeros(std::string&);
std::string diff_time_codes(std::string, std::string, double (*)(double, double));
#endif
