#include <iostream>
#include <vector>
#include "utils.h"
using namespace std;

int returnSeconds(string l){
	string valid="0123456789:";
	vector<string> lvec=string_split(l,':');
	if(lvec.size()!=3)
		return -1;
	int h=stoi(lvec[0]);
	int m=stoi(lvec[1]);
	double s=stod(lvec[2]);
	s+=(m*60+(h*60*60));
	int s_int=s;
	return s_int;
}

int main(int argc, char *argv[]){
	if (argc != 2){
		cout << "not enough arguments\n";
		return 0;
	}
	cout << returnSeconds(string(argv[1]));
}
