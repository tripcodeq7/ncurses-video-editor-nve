#include <ncurses.h>
#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include "utils.h"
#include "fileData.h"
using namespace std;

bool isTimeCode(string t){
	string s="0123456789:.";
	for(int i=0;i<t.length();i++){
		bool isValid=false;
		for(int j=0;j<s.length();j++)
			if(s[j]==t[i])
				isValid=true;
		if(!isValid)
			return false;
			}
		return true;
}

/*This code replaces the operand lines with proper tags for start and end.
It also corrects mistakes like 0:0 as a time code*/
void opPreProcess(vector<string>& operands, vector<fileData>& files){
	for(int i=0; i<operands.size();i++){
		if(operands[i]=="start")
			operands[i]="0:0:0";
		//Replaces the end keyword with the duration
		if(operands[i]=="end" && operands.size()>=2)
			for(int j=0;j<files.size();j++)
				if(operands[1]==files[j].strFileName)
					operands[i]=files[j].strFileInfo;
		if(operands[i].find("last")!=-1){
			string str="";
			for(int j=4;j<operands[i].length();j++)
				str+=operands[i][j];
			str+="_-1.mkv";
			operands[i]=getLastFile(files,str);
		}
		//Checks and fixes time code format
		for(int j=0;j<operands[i].length();j++)
			if(isTimeCode(operands[i]))
				while(string_split(operands[i],':').size()<3)
					operands[i]=operands[i].insert(0,"0:");
			
	}	
}
