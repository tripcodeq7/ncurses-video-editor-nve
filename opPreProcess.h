#ifndef OPPREPROCESS_H
#define OPPREPROCESS_H
#include <string>
#include <vector>
#include <fstream>
#include "utils.h"
#include "fileData.h"
void opPreProcess(std::vector<std::string>& operands, std::vector<fileData>& files);
bool isTimeCode(std::string t);
#endif
