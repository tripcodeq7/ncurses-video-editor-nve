#include <ncurses.h>
#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include "utils.h"
#include "fileData.h"
using namespace std;

fileData::fileData(string input,string directory){
	highlight=false;
	delay = 0;
        strFileName=string_split(input,'|')[0];
        strFileName.erase(0,2);
        strFileInfo=string_split(input,'|')[1];
        strFileDirectory=directory;
}
string fileData::print(){
        string out=strFileName+" "+strFileInfo;
        removeTrailingZeros(out);
        return out+"\n";
}

string getLastFile(vector<fileData> files,string fileName){
	vector<string> fileNames;
	string numbers="-0123456789";
	string strFileNumber="";
	string prefix=string_split(fileName,'_')[0];
	for(int i=0;i<files.size();i++)
		if(files[i].strFileDirectory=="work" && prefix==string_split(files[i].strFileName,'_')[0])
			fileNames.push_back(files[i].strFileName);
	if(fileNames.size()==0)
		return fileName;
	string lastFile=fileNames[0];
	for(int i=0;i<fileNames.size();i++)
		if(stoi(string_split(string_split(lastFile,'_')[1],'.')[0]) < stoi(string_split(string_split(fileNames[i],'_')[1],'.')[0]) )
			lastFile=fileNames[i];
	return lastFile;	
}

string incrementFile(string fileName){
	return string_split(fileName,'_')[0]+"_"+to_string(stoi(string_split(string_split(fileName,'_')[1],'.')[0])+1)+"."+string_split(fileName,'.')[1];
}

